function RI = RI_Calculator(FADataInput, column_Fx, column_Z, FDistQuants)

%% Arguments:
% FADataInput = Cell Array of Focal Adhesion Data
% column_Z = index of column storing Z
% column_Fx = index of column storing Fx
% FDistQuants = low and high quartile based on Fdist

data = FADataInput(:,[column_Fx column_Z]);

%Compute Normalized Z for the cell
data(:,2) = num2cell(NormZ_Calculator(data(:, 2), 1));

% Convert to matrix
data = cell2mat(data);

% Retrieve low and high force quartiles for the cell
Quants = FDistQuants;
LowQuantVal = Quants(1);
HighQuantVal = Quants(2);
LowForceQuantile = data(data(:, 1) <= LowQuantVal, :);
HighForceQuantile = data(data(:, 1) >= HighQuantVal, :);

% Compute mean NormZ_HighF and mean NormZ_LowF
k1 = mean(HighForceQuantile(:,2));
k2 = mean(LowForceQuantile(:,2));

% Compute RI
RI = log(k1/k2)/log(2);

end