function A = getDoubleVector(Pstr)

if isnumeric(Pstr)
    A = Pstr;
else
    if strcmpi(Pstr,'[0,0.01,0.05,0.1:0.1:0.9,0.95,0.99,1]') 
        A = [0,0.01,0.05,0.1:0.1:0.9,0.95,0.99,1];
    elseif strcmpi(Pstr,'[0,0.005,0.025,0.05:.05:.45,0.475,0.495,0.5]')
        A = [0,0.005,0.025,0.05:.05:.45,0.475,0.495,0.5];
    elseif strcmpi(Pstr,'[0 0.5]')
        A = [0 0.5];
    elseif strcmpi(Pstr,'[0:0.25:1]')
        A = [0:0.25:1];
    elseif strcmpi(Pstr,'[0.05,0.5,0.95]')
        A = [0.05 0.5 0.95];
    end
end

end