function C = fastismember(A,B)
C = sum(xor(A,B),2);
if any(C)
    C = ~logical(C./max(C));
else
    C(:,1) = 1;
    C = logical(C);
end

