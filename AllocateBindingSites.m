function FAdata = AllocateBindingSites(FA,nBS,BSa)
% Allocates binding sites to X, Y, Z
% 'identity'  means specific subsets of binding sites are available to X, Y, Z
% 'universal' means all binding sites are available to all molecules

if strcmpi(BSa,'identity')
    % Randomly set the number of binding sites available to X, Y, Z
    %     pBSx   = percentage of binding sites available to X
    %     pBSy   = percentage of binding sites available to Y
    %     pBSz   = percentage of binding sites available to Z
    % (the minimum number of binding sites available to any component = 10%)
    % (normalize to 100%)
    % (convert to actual number based on nBS)
    % (if statement corrects for rounding errors)
    pBSx = rand(1)+0.10;
    pBSy = rand(1)+0.10;
    pBSz = rand(1)+0.10;
    spBS = sum([pBSx,pBSy,pBSz]);
    pBSx = pBSx./spBS;
    pBSy = pBSy./spBS;
    pBSz = pBSz./spBS;
    nBSx = round(pBSx.*nBS);
    nBSy = round(pBSy.*nBS);
    nBSz = round(pBSz.*nBS);
    sBS = sum([nBSx,nBSy,nBSz]);
    if ~isequal(nBS,sBS)
        nBSx = nBSx+(nBS-sBS);
    end
    % Populate matrix with binding site availability
    tmp(:,1) = vertcat(ones([nBSx,1]).*1,ones([nBSy,1]).*2,ones([nBSz,1]).*3);
    tmp(:,1) = tmp(randperm(nBS),1); % suffle binding sites
    FA(tmp==1,1,:) = 1; % col1 = availability to X
    FA(tmp==2,2,:) = 1; % col2 = availability to Y
    FA(tmp==3,3,:) = 1; % col3 = availability to Z
    FAdata = FA;
    clear tmp
elseif strcmpi(BSa,'universal')
    FA(:,1,:) = 1;
    FA(:,2,:) = 1;
    FA(:,3,:) = 1;
    FAdata = FA;
end
end

