function MCsimMoleculesCall_UPDATE_NoSeeding_ConstantOff_FUNC(iDetails,iFunction,iForceCorr,iCellN,iSystemSize,iPx,iPy,iPz,iPzX,iPzY,iPzFx,iPzFy)
%% Description
% Script designed to perform Monte-Carlo based simulations of FRET-coloc
% data and predict in a random iterative fashion the stoichiometry of
% molecules X, Y, and Z in multiple focal adhesions. In a simplest possible
% scenario, X and Y are two non-interacting proteins within FAs (ex.
% vinculin and ILK), both of which drive the association of Z (some other
% protein of interest, POI) via force-dependent and force-independent
% interactions/recruitment mechanisms.

%%%%% INPUT: Physical Scenario %%%%%
% Cellular Variables
%     nFA  = number of total focal adhesions within the cell
% Random Molecular Variables (will be different for each FA):
%     nBS    = number of total binding sites
%              (can be thought of as number of integrins)
%              (randomly drawn from distribution of
%               measured acceptor intensities, Vdist)
%     F      = mean of the distribution of forces
%              (randomly drawn from distribution of
%               measured vinculin tensions, Fdist)
%     Fvec   = Force across individual molecules X or Y
%              (randomly drawn from narrower distribution <P(Fvec)> = F)

%%%%% INPUT: Molecular Recruitment and Interactions %%%%%
% Random association and disassociation of X, Y, and Z
%     PxOn  = probability of random association of X to X binding site
%     PxOff = probability of random disassociation of X from X binding site
%     PyOn  = probability of random association of Y to Y binding site
%     PyOff = probability of random disassociation of Y from Y binding site
%     PzOn  = probability of random association of Z to Z v site
%     PzOff = probability of random disassociation of Z from Z binding site
% Force-Independent and Force-Dependent recruitment of Z
%     Px  - probability that Z will bind a free X
%            (describes the sensitivity of force-indep. assoc. of Z to X)
%     Py  - probability that Z will bind a free Y
%            (describes the sensitivity of force-indep. assoc. of Z to Y)
%     Pxf  - probability that Z will bind a free X
%            (describes the sensitivity of force-dep. assoc. of Z to X)
%     Pyf  - probability that Z will bind a free Y
%            (describes the sensitivity of force-dep. assoc. of Z to Y)

%%%%% OUTPUT: FAdata %%%%%
% FAdata contains timecourse data for protein recruitment of molecules X,
% Y, and Z to a number of FAs (nFA), stored as a matrix with the following
% columns:
% col1     = "Details" (user input details about the simulation experiment they just ran)
% col2     = "Style" (dictates which protein-protein interactions occur through...should be named according to the Rules Matrix)
% col3     = "BSavaiability" (either universal or identity)
% col4     = "Function" (describing the relationship between P(F) and F
% col5     = Noise (level of noise in the simulation, higher noise = fewer molecules in the simulation)
% col6-19  = Binding and unbinding probabilities
%        NOTE: all unbinding probabilities are one minus the binding probability
%        EX:   P(unbind) = 1 - P(bind)
%               Px    X binding surface
%               Py    Y binding surface
%               Pz    Z binding surface
%               Pzx   Z binding X in a force-independent manner
%               Pzy   Z binding Y in a force-independent manner
%               Pzfx  Z binding X in a force-dependent manner (depends on Fx)
%               Pzfy  Z binding Y in a force-dependent manner (depends on Fy)
% col20-22 = binding site availability to X, Y, Z
% col23-25 = binding site occupation by X, Y, Z (random)
% col26-27 = secondary recruitment of molecule Z (force-independent recruitment by X, Y)
% col28-29 = secondary recruitment of molecule Z (force-dependent recruitment by X, Y)
% col30    = force across individual FA (a.u., normalized such that max(F) = 1)
% col31    = FA ID (unique ID for a single FA with nBS binding sites)
% col32    = Timepoint (a.u.)
% col33    = cumulative X molecules
% col34    = cumulative Y molecules
% col35    = cumulative Z molecules (total)
% col36    = Z/X ratio
% col37    = Z/Y ratio
% col38    = Y/X ratio

%%%%% NOTES %%%%%
% For now, we're assuming an infinite cytosolic pool of X, Y, and Z

% %% Clean Up Workspace
% clc
% clear
% close all

%% User Input Parameters
% Physical Scenario and Saving
params.details = iDetails;
% [UPDATE] Remove// params.noise = 20;
params.systemSize = iSystemSize; % [UPDATE] System Size is the Max nBS for a FA.  250 roughly corresponds to normal size in previous simulations (Noise = 20).
params.style = 'AllReversible';
params.BSavailability = 'Universal'; % options: 'Identity' or 'Universal'
params.function = iFunction; % 'Step' or 'Slope' or 'Nonlin' or 'Slope X_Neg Y_Pos'
params.FxFyCorr = iForceCorr;
params.nFA = 100;
params.tpnts = 250;
params.ExampleMCvisualization = 'n';
params.ExampleSCvisualization = 'n';
params.Timecourse = 'n';
params.savefolder = fullfile('',[params.details]);
% params.savefolder = fullfile('../FRET-Coloc-Correlation-Analysis',[params.details]);
params.randnum = [1:iCellN];

% [UPDATE] Define Force Quartiles used to compute Recruitment Index
% Set to constant values based on the uniform force distribution
Fdistquartiles = [0.25 0.75];

% Binding Probabilities
% params.pNames = {'Xon','Xoff','Yon','Yoff','Zon','Zoff','Zxon','Zxoff','Zyon','Zyoff','Zxfon','Zxfoff','Zyfon','Zyfoff'};
params.pNames = {'Px','Py','Pz','Pzx','Pzy','Pzfx','Pzfy'};
params.pVals = {...
    iPx,... %Px
    iPy,... %Py
    iPz,... %Pz
    iPzX,... %Pzx
    iPzY,... %Pzy
    iPzFx,... %Pzfx
    iPzFy};   %Pzfy

%% Constant Input Parameters
if exist(params.savefolder,'dir')==7
    error('Choose a new place to save this simulation or delete the old one')
else
    mkdir(params.savefolder);
end
addpath(genpath(pwd));

% [UPDATE] Forces are now drawn from a continuous uniform distribution within MCsimMolecules_UPDATE
% [UPDATE] Remove// UniformForceDistribution;
% [UPDATE] Remove// params.Fdist = Fdist;
% [UPDATE] Remove// Fdistquartiles = quantile(Fdist, [.25 .75]);

% [UPDATE] nBS are now drawn from a continuous uniform distribution within
% MCsimMolecules_UPDATE.  System Size is the Max nBS for a FA.
% [UPDATE] Remove// UniformVenusDistribution;
% [UPDATE] Remove// params.Vdist = Vdist;
% [UPDATE] Remove// params.Vdist = round(params.Vdist.*params.systemSize,0);

%% Compile all combinations of parameters
params.comb_params = combvec(params.pVals{:});
params.comb_params = params.comb_params';
[n_its,~] = size(params.comb_params);

%% MC simulations on each FA with each combination of parameters
count = 0;
for r = 1:length(params.randnum)
    % [UPDATE] Remove// rng(params.randnum(r));
    params.savename = strcat(params.details,'_Cell',num2str(r));
    for i = 1:n_its
        count = count + 1;
        [preFAdata,preSCdata] = MCsimMolecules_UPDATE_NoSeeding_ConstantOff(params,i,r);
        
        if i==1
            FAdata = preFAdata;
            SCdata = preSCdata;
        else
            FAdata = vertcat(FAdata,preFAdata);
            SCdata = vertcat(SCdata,preSCdata);
        end
        % Simple MC data output visualization
        if strcmpi(params.ExampleMCvisualization,'y') && params.randnum(r)==368
            ExampleMCvisualization(preFAdata,params,i)
        end
        if strcmpi(params.ExampleSCvisualization,'y') && params.randnum(r)==368
            ExampleSCvisualization(preSCdata,params,i)
        end
     
        clc
        disp([num2str((count/(n_its*length(params.randnum)))*100), '% complete']);
        clear preFAdata preSCdata
    end
    
    %% Save Compiled TimeCourse Results
    % Final calculations of total X, Y, Z molecules and ratios in individual FAs
    FAdata(:,28:33) = zeros([length(FAdata),6]);
    FAdata(:,28) = FAdata(:,18); % total X
    FAdata(:,29) = FAdata(:,19); % total Y
    FAdata(:,30) = sum(FAdata(:,20:24),2); % total Z
        
    %%%%% Baseline Imaging Noise Estimate - Andy, 18 JAN 2019
    FAdata(:,28) = FAdata(:,28)+(params.systemSize/250)*(1+round(2*rand(size(FAdata(:,28)))));
    FAdata(:,29) = FAdata(:,29)+(params.systemSize/250)*(1+round(2*rand(size(FAdata(:,29)))));
    FAdata(:,30) = FAdata(:,30)+(params.systemSize/250)*(1+round(2*rand(size(FAdata(:,30)))));
    %%%%%
    
    nzX = FAdata(:,28)>0; % nonzero X values
    FAdata(nzX,31) = FAdata(nzX,30)./FAdata(nzX,28); % Z/X for nonzero X values
    nzY = FAdata(:,29)>0; % nonzeros Y values
    FAdata(nzY,32) = FAdata(nzY,30)./FAdata(nzY,29); % Z/Y for nonzero Y values
    FAdata(nzX,33) = FAdata(nzX,29)./FAdata(nzX,28); % Y/X for nonzero X values
    FAdata = horzcat(repmat(params.randnum(r),[length(FAdata),1]),repmat(params.systemSize,[length(FAdata),1]),FAdata); % Noise level
    % Definition of Headers in FA data
    headers = {'Details','Style','BSavailability','Function','RandN','System Size',...
        'Px_on','Px_off','Py_on','Py_off','Pz_on','Pz_off','Pzx_on','Pzx_off',...
        'Pzy_on','Pzy_off','Pzfx_on','Pzfx_off','Pzfy_on','Pzfy_off',...
        'AvailX','AvailY','AvailZ','OccX','OccY','OccZ',...
        'Z_ForceIndX','Z_ForceIndY','Z_ForceDepX','Z_ForceDepY',...
        'Force','FA_ID','Timepoint','X','Y','Z','Z/X','Z/Y','Y/X'};
    
    % to .txt file (these will be large, so maybe only save them if they are
    % single runs (not multiparametric analyses)
    if strcmpi(params.Timecourse,'y')
        save(fullfile(params.savefolder,['timecourse_' params.savename '.txt']),'FAdata','-ascii');
    end
    
    % Save parameters used to generate the data
    save(fullfile(params.savefolder,['SimParams_' params.savename '.mat']),'-struct','params');
    
    % Save just the final state of the FAs to .xls file
    subrows = FAdata(:,29)==params.tpnts+1;
    [nFAsT,~] = size(FAdata);
    nFAsT = nFAsT/(params.tpnts+1);
    FAdata1 = num2cell(FAdata(subrows,:));
    FAdata1 = horzcat(...
        repmat({params.details},[nFAsT,1]),...
        repmat({params.style},[nFAsT,1]),...
        repmat({params.BSavailability},[nFAsT,1]),...
        repmat({params.function},[nFAsT,1]),...
        FAdata1);
    FAdata1 = vertcat(headers,FAdata1);
    xlswrite(fullfile(params.savefolder,['subset_' params.savename '.xlsx']),FAdata1);
    
    % [UPDATE] Compute Cell Summary Data
    % Compute for each parameter combination in the current cell
    for k=1:n_its
        first_FA = 2 + (k-1)*params.nFA;
        last_FA = 1 + k*params.nFA;
        Column_Force = 31;
        Column_X = 34;
        Column_Z = 36;
        SingleSimulationFAdata = FAdata1(first_FA:last_FA,:);
        CellSummary_Data((count-n_its)+k,:) = {SingleSimulationFAdata{1,1:20},...
                RI_Calculator(SingleSimulationFAdata, Column_Force, Column_Z,Fdistquartiles),...
                Correlation_Calculator_XZ(SingleSimulationFAdata, Column_X, Column_Z),...
                Correlation_Calculator_FZ(SingleSimulationFAdata, Column_Force, Column_Z)};
    end
    
end

%% Compile All Cell Data into Final Data Structure
files = file_search('\w+.xlsx',params.savefolder);
for r = 1:length(params.randnum)
    d = importdata(fullfile(params.savefolder,files{r}));
    if r==1
        [nr,nc] = size(d.textdata);
        nr = nr-1;
        txtcols = 1:4;
        numcols = 5:nc;
        outdata = cell([nr+1,nc]);
        outdata(1,:) = d.textdata(1,:);
    end
    outdata(r*nr+2-nr:r*nr+1,numcols) = num2cell(d.data);
    outdata(r*nr+2-nr:r*nr+1,txtcols) = d.textdata(2:end,txtcols);
end
xlswrite(fullfile(params.savefolder,['ods_' params.savename '.xlsx']),outdata);

% Cell Summary Data: Append headers and write to new excel file
Headers_CellSummaryData = {FAdata1{1,1:20},...
    'RI',...
    'Corr Coeff X-Z',...
    'Corr Coeff Fx-Z'};
CellSummaryData = vertcat(Headers_CellSummaryData, CellSummary_Data);
filename2 = strcat(params.savefolder,'/','CellSummaryData.xlsx');
xlswrite(filename2,CellSummaryData,1);

%% Clean up
rmpath(genpath(pwd))
close all
disp('Simulation Complete');

end