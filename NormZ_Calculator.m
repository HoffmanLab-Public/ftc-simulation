function NormZ = NormZ_Calculator(FADataInput, column_Z)

%% Arguments:
% FADataInput = Cell Array of Focal Adhesion Data
% column_Z = index of column storing Z

data = cell2mat(FADataInput(:,column_Z));

CellMeanCy5Intensity = mean(data(:));

NormZ = (data/CellMeanCy5Intensity);

end