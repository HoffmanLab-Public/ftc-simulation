function PCC_NormX_NormZ = Correlation_Calculator_XZ(FADataInput, column_X, column_Z)

%% Description
% Computes the Pearson's Linear Correlation Coefficient between:
% NormX (Venus Intensity) and NormZ (POI Stain Intensity)
% Arguments: 
% FADataInput = Cell Array of Focal Adhesion Data
% column_X = index of column storing X
% column_Z = index of column storing Z

data = FADataInput(:,[column_X column_Z]);

% Compute Normalized X and Z for the cell
data(:,1) = num2cell(NormZ_Calculator(data(:, 1), 1));
data(:,2) = num2cell(NormZ_Calculator(data(:, 2), 1));

% Convert to matrix
data = cell2mat(data);

% Compute PCC_F_NormZ
NormX = data(:, 1);
NormZ = data(:,2);
PCC_NormX_NormZ = corr(NormX,NormZ, 'Type','Pearson');

end