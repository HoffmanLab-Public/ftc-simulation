 function probabilities = probs(p)
%Function calculates the probabilities of mutually exclusive events
%occuring (or an extra option that none of them occur).
none = prod(1-p,2);
probabilities = ((1-none).*p)./sum(p,2);
probabilities(:,end+1) = none;
probabilities = cumsum(probabilities,2);
end

