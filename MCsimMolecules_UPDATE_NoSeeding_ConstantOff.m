function [FAdata,SCdata] = MCsimMolecules_UPDATE_NoSeeding_ConstantOff(params,iteration,randnumitt)
%% Description
% See MCsimMolecules for details on function inputs and outputs. Briefly,
% this function is designed to perform Monte Carlo simulations of FA
% recruitment of molecules X, Y, & Z and saves the output to a matrix
% called FAdata. Multiple scenarios that describe the molecular
% interactions between X, Y, & Z can be imagined and are defined by the
% rules in the "rules matrix"

%% Simplify inputs
% Physical parameters
nFA = params.nFA; % How many focal adhesions?
tpnts = params.tpnts; % How many timepoints?
BSa = params.BSavailability; % How is binding site availability set?
fxn = params.function; % what function dictates the relationship between PZ(F) and F?
corr = params.FxFyCorr; % are Fx and Fy correlated?
pVals = params.comb_params(iteration,:);
pVals = repelem(pVals,2);% Duplicate all on rates to be off rates too

% [UPDATE] System Size is the Max nBS for a FA
systemsize = params.systemSize;

% 1: Constant off rate = 0.5
pVals(2:2:end)=0.5;

% 2: P(off) = 1-P(on)
% pVals(2:2:end) = 1-pVals(1:2:end);

% 3: P(off) = 1-P(on) with a minimum of 0.1
% pVals(2:2:end) = 1-pVals(1:2:end);
% r1 = pVals<0.1;
% r2 = logical([0 1 0 1 0 1 0 1 0 1 0 1 0 1]);
% r = r1 & r2;
% pVals(r) = 0.1;

% [UPDATE] Remove// Vdist = params.Vdist; % Intensity Distribution
% [UPDATE] Remove// Fdist = params.Fdist; % Force Distribution
style = params.style; % Style of simulation

% [UPDATE] Remove// rng(params.randnum(randnumitt));

% Matrices to populate with simulation data
% FAo = cell([nFA,1]);
FAdata = zeros([nFA.*(tpnts+1),28]);
FAdata(:,1:14) = repmat(pVals,[nFA.*(tpnts+1),1]);
SCdata = zeros([10*nFA,12]);

%% Simulate association of X, Y, & Z to individual FAs over time
for i = 1:nFA
    
    % [UPDATE] Set number of binding sites in the FA
    % Draw from Continuous Uniform Distribution on [0,systemsize].
    % Round to an integer.  
    % Add 1 so that min(nBS)=1.
    nBS = 1 + round(systemsize*rand());
    % WAS: nBS  = max(1,round(Vdist(ceil(rand(1).*length(Vdist)))));
    
    % Preallocate space to save molecule binding data and assign binding
    % site availability to X, Y, Z molecules
    FA = zeros([nBS,4,(tpnts+1)]);
    SC = zeros([10,10,(tpnts+1)]);
    FA(:,4,:) = 1; % All binding sites start in unoccupied state (state 1)
    FA = AllocateBindingSites(FA,nBS,BSa);
    
    % [UPDATE] Set Fx and Fy in each FA
    % Draw mean forces from Continuous Uniform Distribution on [0,1]
    AverageForce_X = rand();
    AverageForce_Y = rand();
    % Set forces at each binding site in the FA
    % These are normally distributed about the selected AverageForce
    % Edges are treated by reflection
    stdF1 = 0.05;
    Fvec1 = AverageForce_X + stdF1.*randn([nBS,1]);
    Fvec1(Fvec1<0) = abs(Fvec1(Fvec1<0));
    Fvec1(Fvec1>1) = 2-Fvec1(Fvec1>1);
    stdF2 = 0.05;
    Fvec2 = AverageForce_Y + stdF2.*randn([nBS,1]);
    Fvec2(Fvec2<0) = abs(Fvec2(Fvec2<0));
    Fvec2(Fvec2>1) = 2-Fvec2(Fvec2>1);
    % Assemble Fvec
    Fvec = horzcat(Fvec1,Fvec2); 
    
    % WAS - Previous Uniform FDist w/ Constant Std Dev
% %     F1  = Fdist(ceil(rand(1).*length(Fdist)));
% %     stdF1 = .1;
% %     Fvec1 = F1+stdF1.*randn([nBS,1]);
% %     Fvec1 = Fvec1./max(Fdist);
% %     Fvec1(Fvec1<0) = 0;
% %     Fvec1(Fvec1>1) = 1;
% %     F2  = Fdist(ceil(rand(1).*length(Fdist)));
% %     stdF2 = .1; 
% %     Fvec2 = F2+stdF2.*randn([nBS,1]);
% %     Fvec2 = Fvec2./max(Fdist);
% %     Fvec2(Fvec2<0) = 0;
% %     Fvec2(Fvec2>1) = 1;
% %     Fvec = horzcat(Fvec1,Fvec2);
    
    % WAS WAS - Previous Data-Informed FDist w/ Nonconstant Std Dev
% %     % Draw a random force from the measured distribution of vinculin tensions
% %     % (stdF calculated from fit of F - stdF relationship)
% %     % (normalize force to be between 0 and 1)
% %     F1  = Fdist(ceil(rand(1).*length(Fdist)));
% %     stdF1 = 0.731*F1-0.285*F1^2+0.03764*F1^3;
% %     Fvec1 = abs(F1+stdF1.*randn([nBS,1]));
% %     Fvec1 = Fvec1./max(Fdist);
% %     F2  = Fdist(ceil(rand(1).*length(Fdist)));
% %     stdF2 = 0.731*F2-0.285*F2^2+0.03764*F2^3;
% %     Fvec2 = abs(F2+stdF2.*randn([nBS,1]));
% %     Fvec2 = Fvec2./max(Fdist);
% %     Fvec = horzcat(Fvec1,Fvec2);
    
    %% Iterate on each FA at each timepoint
    for t = 1:tpnts
        % New rules for step-wise accumulation of molecules
        [FA(:,:,t+1),SC(:,:,t+1)] = ProbabilityAccum1_UPDATE(FA(:,:,t),pVals,Fvec,nBS,fxn,corr,style);
    end
    % Compile Focal Adhesion (FA) data
    FA = reshape(permute(FA,[1,3,2]),[nBS.*(tpnts+1),4]);
    FA(FA(:,4)==1,5:11)  = repmat([0,0,0,0,0,0,0],[length(nonzeros(FA(:,4) ==1)),1]); % empty
    FA(FA(:,4)==2,5:11)  = repmat([1,0,0,0,0,0,0],[length(nonzeros(FA(:,4) ==2)),1]); % X
    FA(FA(:,4)==3,5:11)  = repmat([0,1,0,0,0,0,0],[length(nonzeros(FA(:,4) ==3)),1]); % Y
    FA(FA(:,4)==4,5:11)  = repmat([0,0,1,0,0,0,0],[length(nonzeros(FA(:,4) ==4)),1]); % Z
    FA(FA(:,4)==5,5:11)  = repmat([1,0,0,1,0,0,0],[length(nonzeros(FA(:,4) ==5)),1]); % ZXi
    FA(FA(:,4)==6,5:11)  = repmat([0,1,0,0,1,0,0],[length(nonzeros(FA(:,4) ==6)),1]); % ZYi
    FA(FA(:,4)==7,5:11)  = repmat([1,0,0,0,0,1,0],[length(nonzeros(FA(:,4) ==7)),1]); % ZXd
    FA(FA(:,4)==8,5:11)  = repmat([0,1,0,0,0,0,1],[length(nonzeros(FA(:,4) ==8)),1]); % ZYd
    FA(FA(:,4)==9,5:11)  = repmat([1,0,0,1,0,1,0],[length(nonzeros(FA(:,4) ==9)),1]); % XZZ
    FA(FA(:,4)==10,5:11) = repmat([0,1,0,0,1,0,1],[length(nonzeros(FA(:,4)==10)),1]); % YZZ
    FA = permute(reshape(FA',[11,nBS,tpnts+1]),[2,1,3]);
    rws = i*(tpnts+1)-tpnts:i*(tpnts+1);
    FAdata(rws,15:25) = reshape(permute(sum(FA,1),[1,3,2]),[tpnts+1,11]);
    FAdata(rws,26) = mean(Fvec(:,1)); % [NEEDS REVIEW] Force is the average of force across BS, which is not necessarily the mean used to generate the forces. 
    FAdata(rws,27) = i;
    FAdata(rws,28) = 1:(tpnts+1);
    
    % Compile State Change (SC) data
    srows = i*10-9:i*10;
    SCdata(srows,1:10) = sum(SC(:,:,1:tpnts),3); % state changes
    SCdata(srows,11) = i; % FA ID
    SCdata(srows,12) = mean(Fvec(:,1)); % Force
end
FAdata(:,18) = [];
end

