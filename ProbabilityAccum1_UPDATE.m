function [FA,SC] = ProbabilityAccum1_UPDATE(FA,pVals,Fvec,nBS,fxn,corr,style)
% Describes random accumulation of X, Y, and Z to available binding sites
% FA data is a structure whose length is the number of binding sites in a
% given FA and contains 4 columns describing a particular binding site's:
% col 1  = Availability to X
% col 2  = Availability to Y
% col 3  = Availability to Z
% col 4  = Occupation State

%% Only consider subset of binding sites at each timepoint (rule0)
[mol,~] = size(FA);
smol = round(mol/10);

%% Generate random numbers, probabilities, states
[tm,nstates] = TMgen(style);
SC = zeros(nstates);
rule0 = [ones(smol,1);zeros((mol-smol),1)];
rule0 = logical(rule0(randperm(mol),1));

for i = randperm(nstates)
    rows = FA(:,4)==i;
    rows = rows & rule0;
    pValLoc = nonzeros(tm(i,:));
    c = pVals(pValLoc);
    tmp = zeros([nBS,length(c)]);
    for k = 1:length(c)
        tmp(:,k) = repmat(c(k),[nBS,1]);
        
        %If Transition is Binding to Force-Dep Site
        if pValLoc(k)==11 || pValLoc(k)==13 
            % If Binding to Force-Dep Site on Y & Forces Not Correlated
            if strcmpi(corr,'n') && (i == 3 || i == 6)
                if strcmpi(fxn,'X_Slope+ Y_Slope+')||strcmpi(fxn,'X_Slope- Y_Slope+')||strcmpi(fxn,'X_Constant Y_Slope+')
                    tmp(:,k) = tmp(:,k).*Fvec(:,2);
                elseif strcmpi(fxn,'X_Slope+ Y_Slope-')||strcmpi(fxn,'X_Slope- Y_Slope-')||strcmpi(fxn,'X_Constant Y_Slope-')
                    tmp(:,k) = tmp(:,k).*(1-Fvec(:,2));
                else
                    error('Invalid Force Function');
                end
            % If Binding to Force-Dep Site on Y & Forces Correlated
            elseif strcmpi(corr,'y') && (i == 3 || i == 6)
                if strcmpi(fxn,'X_Slope+ Y_Slope+')||strcmpi(fxn,'X_Slope- Y_Slope+')||strcmpi(fxn,'X_Constant Y_Slope+')
                    tmp(:,k) = tmp(:,k).*Fvec(:,1);
                elseif strcmpi(fxn,'X_Slope+ Y_Slope-')||strcmpi(fxn,'X_Slope- Y_Slope-')||strcmpi(fxn,'X_Constant Y_Slope-')
                    tmp(:,k) = tmp(:,k).*(1-Fvec(:,1));
                else
                    error('Invalid Force Function');
                end
            % If Binding to Force-Dep Site on X
            else
                if strcmpi(fxn,'X_Slope+ Y_Slope+')||strcmpi(fxn,'X_Slope+ Y_Slope-')
                    tmp(:,k) = tmp(:,k).*Fvec(:,1);
                elseif strcmpi(fxn,'X_Slope- Y_Slope+')||strcmpi(fxn,'X_Slope- Y_Slope-')
                    tmp(:,k) = tmp(:,k).*(1-Fvec(:,1));
                elseif strcmpi(fxn,'X_Constant Y_Slope+')||strcmpi(fxn,'X_Constant Y_Slope-')
                    tmp(:,k) = tmp(:,k);
                else
                    error('Invalid Force Function');
                end
            end 
        end
    end
    E = probs(tmp);
    E = [zeros(nBS,1),E];
    FutureState = find(tm(i,:)>0);
    r = rand([nBS,1]);
    for j = randperm(length(FutureState))
        if i==1
            Arows = FA(:,FutureState(j)-1)==1; % if the BS (j-1 occupation states before the actual binding event) is empty (i=1) then only consider adding Xs to X binding sites, Y to Y binding sites, Z to Z binding sites
            Srows = length(nonzeros(Arows & rows & r>=E(:,j) & r<E(:,j+1)));
            if Srows>0
                FA(Arows & rows & r>=E(:,j) & r<E(:,j+1),4) = FutureState(j);
                SC(i,FutureState(j))=Srows;
                SC(i,i) = length(nonzeros(Arows & rows))-Srows;
            end
        else
            Srows = length(nonzeros(rows & r>=E(:,j) & r<E(:,j+1)));
            if Srows>0
                FA(rows & r>=E(:,j) & r<E(:,j+1),4) = FutureState(j);
                SC(i,FutureState(j))=Srows;
                SC(i,i) = length(nonzeros(rows))-Srows;
            end
        end
    end
end
end