% This script runs multiple simulations
% Global parameters for all simulations in this set are hard coded

clc;
clear;
close all;

[~,~,MultiSimLog] = xlsread('MultiSimLog_Test.xlsx',1);
    col_ID = 1;
    col_Function = 5;
    col_ForceCorr = 6;
    col_RNGSeeding = 7;
    col_CellN = 8;
    col_SystemSize = 9;
    col_Px = 10;
    col_Py = 11;
    col_Pz = 12;
    col_PzX = 13;
    col_PzY = 14;
    col_PzFx = 15;
    col_PzFy = 16;

for i = 2:length(MultiSimLog(:,1))
    Details = strcat('SIM_',num2str(MultiSimLog{i, col_ID}));
    Function = MultiSimLog{i, col_Function};
    ForceCorr = MultiSimLog{i, col_ForceCorr};
    RNGSeeding = MultiSimLog{i, col_RNGSeeding};
    CellN = MultiSimLog{i,  col_CellN};
    SystemSize = MultiSimLog{i, col_SystemSize};
    Px = getDoubleVector(MultiSimLog{i, col_Px});
    Py = getDoubleVector(MultiSimLog{i, col_Py});
    Pz = getDoubleVector(MultiSimLog{i, col_Pz});
    PzX = getDoubleVector(MultiSimLog{i, col_PzX});
    PzY = getDoubleVector(MultiSimLog{i, col_PzY});
    PzFx = getDoubleVector(MultiSimLog{i, col_PzFx});
    PzFy = getDoubleVector(MultiSimLog{i, col_PzFy});
    
    % rng(MultiSimLog{i, col_ID}); %For reproducibility
    
    MCsimMoleculesCall_UPDATE_NoSeeding_ConstantOff_FUNC(Details,Function,ForceCorr,CellN,SystemSize,Px,Py,Pz,PzX,PzY,PzFx,PzFy);
end
