function PCC_F_NormZ = Correlation_Calculator_FZ(FADataInput, column_Fx, column_Z)

%% Description
% Computes the Pearson's Linear Correlation Coefficient between:
% Fx (Force Intensity) and Z (POI Stain Intensity)
% Arguments: 
% FADataInput = Cell Array of Focal Adhesion Data
% column_Fx = index of column storing Fx
% column_Z = index of column storing Z

data = FADataInput(:,[column_Fx column_Z]);

% Compute Normalized Z for the cell
data(:,2) = num2cell(NormZ_Calculator(data(:, 2), 1));

% Convert to matrix
data = cell2mat(data);

% Compute PCC_F_NormZ
F = data(:, 1);
NormZ = data(:,2);
PCC_F_NormZ = corr(F,NormZ, 'Type','Pearson');

end