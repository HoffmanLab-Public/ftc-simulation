% 
% ExcelFile = 'C:\Users\tcsho\Desktop\04 FCC Simulation Data\RESULTS - SET 4\FRET-Coloc-Correlation-Analysis\SIM_1011\ods_SIM_1011_Cell100.xlsx';
% 
% [~,~,AllData] = xlsread(ExcelFile);
% Headers = AllData(1,:);
% Data = AllData(2:end,:);

% Global Constants
numberOfCells = 100;
numberOfFA = 100;
LowForceCutoff = 0.25; % Used for TRI calcs
HighForceCutoff = 0.75; % Used for TRI calcs

indCellid = find(strcmp(Headers,'RandN')); % Cell ID
indFAid = find(strcmp(Headers,'FA_ID')); % FA ID
indFx = find(strcmp(Headers,'Force')); % Force
indZ = find(strcmp(Headers,'Z')); % Z Ind
indX = find(strcmp(Headers,'X')); % X Ind

CellSummaryData = {};
%% For Each Cell: Compute Stats on Low and High Force FA Groups

Cells = unique([Data{:,indCellid}]);
if length(Cells)~=numberOfCells
    error();
end

for nCell = Cells
    thisCell = Data( [Data{:,indCellid}] == nCell, :);
    
    % Find first FA for each simulation done on this cell
    firstFAs = find( [thisCell{:,indFAid}] == 1 );
    nFA = diff(firstFAs);
    if ~all(nFA==numberOfFA)
        error();
    end
    
    % Loop thru each simulation and compute metrics on the set of FAs
    for istart = firstFAs
        thisData = thisCell(istart:istart+numberOfFA-1,:);
        Fx = [thisData{:,indFx}]';
        Z = [thisData{:,indZ}]';
        X = [thisData{:,indX}]';
        
        %% [1] Compute PCC(X,Z)
        PCC_XZ = corr(X,Z,'Type','Pearson');
        
        %% [2] Compute PCC(Fx,Z)
        PCC_FxZ = corr(Fx,Z,'Type','Pearson');
        
        %% [3] Compute ZNorm in High and Low FA Groups for use in TRI
        
        % Compute Cell Mean Z (IF Stain)
        Zmean = mean(Z);

        % Compute Norm Z (Cell Normalized IF Stain)
        Znorm = Z./Zmean;

        % Compute N, Mean, and SEM for: Norm Z of Low Force Group of FA
        LowForceGroup = (Fx<=LowForceCutoff);
        Z_Low = Znorm(LowForceGroup);
        Low_N = length(Z_Low);
        Low_Mean = mean(Z_Low);
        Low_SEM = std(Z_Low)/sqrt(Low_N);

        % Compute N, Mean, and SEM for: Norm Z of High Force Group of FA
        HighForceGroup = (Fx>=HighForceCutoff);
        Z_High = Znorm(HighForceGroup);
        High_N = length(Z_High);
        High_Mean = mean(Z_High);
        High_SEM = std(Z_High)/sqrt(High_N);
        
        TRI_CellLevel = log(High_Mean/Low_Mean)/log(2);
        
        % Store Data for This Simulation for This Cell in a Row of Out1
        CellSummaryData = [CellSummaryData;
            thisData(1,1:20),...
            PCC_XZ, PCC_FxZ,...
            Zmean,...
            Low_N, Low_Mean, Low_SEM,...
            High_N, High_Mean, High_SEM,...
            TRI_CellLevel];
               
    end
      
end

%% Write out Cell Summary Data
HeadersOut = [Headers(1:20),...
                'PCC_XZ', 'PCC_FxZ',...
                'Zmean',...
                'ZnormLow_N', 'ZnormLow_Mean', 'ZnormLow_SEM',...
                'ZnormHigh_N', 'ZnormHigh_Mean', 'ZnormHigh_SEM',...
                'TRI_CellLevel'];
CellSummaryDataOut = [HeadersOut; CellSummaryData];
xlswrite('CellSumData_APR2020.xlsx',CellSummaryDataOut);


%% TRI and Standard Error of TRI for Each 100 Cell Experiment (Simulation)
ExperimentSummaryData = {};

% Find Experiments by looking for the unique parameter combos.
[a,b,c] = unique(cell2mat(CellSummaryData(:,6:20)),'rows');

% Compute Experiment-Level Metrics and SEM
for ii=1:length(b)
    sim=b(ii);
    thisExperiment = CellSummaryData(c==sim,:);
    
    Ncell = length(thisExperiment(:,1));
    
    % [1] PCC(X,Z)
    PCC_XZ = [thisExperiment{:,21}]';
    EXP_MEAN_PCC_XZ = mean(PCC_XZ);
    EXP_SEM_PCC_XZ = std(PCC_XZ)/sqrt(Ncell);
    
    % [2] PCC(Fx,Z)
    PCC_FxZ = [thisExperiment{:,22}]';
    EXP_MEAN_PCC_FxZ = mean(PCC_FxZ);
    EXP_SEM_PCC_FxZ = std(PCC_FxZ)/sqrt(Ncell);
    
    % [3] TRI
    ZnormLow_Mean = mean([thisExperiment{:,25}]');
    ZnormLow_SEM = std([thisExperiment{:,25}]')/sqrt(Ncell);
    ZnormHigh_Mean = mean([thisExperiment{:,28}]');
    ZnormHigh_SEM = std([thisExperiment{:,28}]')/sqrt(Ncell);
    
    EXP_TRI = log( mean(ZnormHigh_Mean)/mean(ZnormLow_Mean) ) / log(2);
    EXP_SEM_TRI = 1/log(2) * sqrt( (ZnormHigh_SEM/ZnormHigh_Mean)^2 + (ZnormLow_SEM/ZnormLow_Mean)^2 );
    
    % [4] Old Way of Comupting TRI: Mean of Single Cell RI's
    CellTRI = [thisExperiment{:,end}]';
    EXP_MEAN_CellTRI = mean(CellTRI);
    EXP_SEM_CellTRI = std(CellTRI)/sqrt(Ncell);
    
    % Store Data for This Simulation for This Cell in a Row of Out1
        ExperimentSummaryData = [ExperimentSummaryData;
            thisExperiment(1,1:20),...
            Ncell,...
            EXP_MEAN_PCC_XZ,...
            EXP_SEM_PCC_XZ,...
            EXP_MEAN_PCC_FxZ,...
            EXP_SEM_PCC_FxZ,...
            EXP_TRI,...
            EXP_SEM_TRI,...
            EXP_MEAN_CellTRI,...
            EXP_SEM_CellTRI];

end

%% Write out Cell Summary Data
HeadersOut = [Headers(1:20),...
                'Ncell',...
                'EXP_MEAN_PCC_XZ',...
                'EXP_SEM_PCC_XZ',...
                'EXP_MEAN_PCC_FxZ',...
                'EXP_SEM_PCC_FxZ',...
                'EXP_TRI',...
                'EXP_SEM_TRI',...
                'EXP_MEAN_CellTRI',...
                'EXP_SEM_CellTRI'];
ExperimentSummaryDataOut = [HeadersOut; ExperimentSummaryData];
xlswrite('ExpSumData_APR2020.xlsx',ExperimentSummaryDataOut);



