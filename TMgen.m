function [tm,nstates] = TMgen(style)
%This function will generate a transition matrix template for a given
%physical scenario, describing the binding and unbinding interactions
%between molecules X, Y, and Z. See 'Simplest.xlsx' in Situations folder
% for visualization. Nonzero values in the transition matrix indicate the
% probability coefficient that governs the transition from current state
% (row @ time = t) to future state(column @ time t = t+1).

% Outputs
%  tm:  transition matrix containing the probabilities of state
%       transitions between time steps t and t + 1, for all t
%  tmf: transition matrix force values containing information on
%       whether forces affect the probability values in the transition
%       matrix (tm)
nstates = 10;
tm = zeros(nstates);
if strcmpi(style,'Simplest')
    tm(1,[2,3,4]) = [1,3,5];
    tm(2,[1,5,7]) = [2,7,11];
    tm(3,[1,6,8]) = [4,9,13];
    tm(4,1)       = 6;
    tm(5,[1,9])   = [2,11];
    tm(6,[1,10])  = [4,13];
    tm(7,[1,9])   = [2,7];
    tm(8,[1,10])  = [4,9];
    tm(9,1)       = 2;
    tm(10,1)      = 4;
elseif strcmpi(style,'AllReversible')
    tm(1,[2,3,4])  = [1,3,5];
    tm(2,[1,5,7])  = [2,7,11];
    tm(3,[1,6,8])  = [4,9,13];
    tm(4,1)        = 6;
    tm(5,[1,2,9])  = [2,8,11];
    tm(6,[1,3,10]) = [4,10,13];
    tm(7,[1,2,9])  = [2,12,7];
    tm(8,[1,3,10]) = [4,14,9];
    tm(9,[1,5,7])  = [2,12,8];
    tm(10,[1,6,8]) = [4,14,10];
elseif strcmpi(style,'Symmetric')
    tm(1,[2,3,4]) = [1,3,5];
    tm(2,[1,5,7]) = [2,7,11];
    tm(3,[1,6,8]) = [4,9,13];
    tm(4,1)       = 6;
    tm(5,[2,9])   = [8,11];
    tm(6,[3,10])  = [10,13];
    tm(7,[2,9])   = [12,7];
    tm(8,[3,10])  = [14,9];
    tm(9,[5,7])   = [12,8];
    tm(10,[6,8])  = [14,10];
else
    error('Choose a valid style for this simulation');
end
end

